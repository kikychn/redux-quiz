1. 创建Home组件，获取笔记数据并展示
2. 将Home绑定到Redux上
3. 创建Notes组件，绑定到Redux上，展示数据
4. 创建NoteDetails组件，绑定到Redux上，展示数据
5. 首页-创建新笔记
6. 笔记详情页-删除笔记
7. 格式校验-新建笔记标题、正文不能为空，提交按钮为disable状态
8. 样式设置