import React, {Component} from 'react';
import {Link} from "react-router-dom";

export class Home extends Component {

  constructor(props, context) {
    super(props, context);

    this.createNote = this.createNote.bind(this);

    this.state = {
      posts:[]
    }
  }

  componentDidMount() {
    fetch('http://localhost:8080/api/posts',{
      method:'GET'
    })
      .then(response=>response.json())
      .then(result=>{
        this.setState({posts:result})
      })
  }

  createNote() {

  }

  render() {
    const linkList = this.state.posts.map((post, index) => {
      return (
        <li key={index} className='notebook'>
          <Link  to={`notes/${post.id}`} >{post.title}</Link>
        </li>
      )
    });

    return (
      <div className='index'>
        <h3>NOTES</h3>
        <ul>
          {linkList}
        </ul>
        <Link to={'/create'} >新增笔记</Link>
      </div>
    );
  }
}
