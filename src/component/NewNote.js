import React from 'react';

const NewNote = () => {
  return (
    <div className="newNote">
      <h3>创建笔记</h3>
      <hr/>
      <h4>标题</h4>
      <input type="text"/>
      <h4>正文</h4>
      <textarea name="content" id="content" cols="30" rows="10"></textarea>
      <br/>
      <input type="text" value="提交"/>
      <input type="text" value="取消"/>
    </div>
  )
};

export default NewNote;