import React, {Component} from 'react';

export class Notes extends Component {

  constructor(props, context) {
    super(props, context);

    this.showPost = this.showPost.bind(this);

    this.state = {
      posts: [],
      post: ''
    };
  }

   componentDidMount() {
     fetch('http://localhost:8080/api/posts',{method:'GET'})
      .then(response=>response.json())
      .then(result=>{
        this.setState(()=>({posts:result}));
        this.setState(()=>({post:result.find(p => p.id == this.props.match.params.id)}))
      })
  }

  showPost(id) {
    this.setState(()=>({post:this.state.posts.find(p => p.id == id)}))
  }

  render() {
    const linkList = this.state.posts.map((post, index) => {
      return (
        <li key={index} onClick={()=>this.showPost(post.id)}>
          <input type="button" value={post.title}/>
        </li>
      )
    });

    return (
      <div>
        <h3>NOTES</h3>
        <ul>
          {linkList}
        </ul>
        <p>{this.state.post.description}</p>
        <input type="button" value="删除"/>
        <input type="button" value="返回"/>
      </div>
    );
  }
}
