const initState = {
  posts:['a','b']
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'SET_POSTS':
      console.log('set posts');
      return {
        ...state,
        posts: action.posts
      };

    default:
      return state
  }
};
