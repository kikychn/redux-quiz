import {combineReducers} from "redux";
import getPostsReducer from "./getPostsReducer";

const reducers = combineReducers({
  getPosts: getPostsReducer
});
export default reducers;