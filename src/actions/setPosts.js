export const setPosts = (path) => (dispatch) => {
  fetch(path,{
    method:'GET'
  })
    .then(response=>response.json())
    .then(result=>{
      dispatch({
        type: 'SET_POSTS',
        posts: result
      });
    })
};