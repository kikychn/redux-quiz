import React, {Component} from 'react';
import './App.less';
import {Home} from "./component/Home";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import {Notes} from "./component/Notes";
import NewNote from "./component/NewNote";

class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router>
          <Switch>
            <Route exact path='/' component={Home} />
            <Route path='/notes/:id' component={Notes} />
            <Route path='/create' component={NewNote} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;